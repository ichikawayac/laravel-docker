
# nginx + php-fpm(laravel) + mysql + redis サーバーを立てる
参考記事
https://qiita.com/meidaimae/items/1b5902e2e520ece34b9a
mcryptがphp7.2から提供されなくなったので編集する必要がある
https://stackoverflow.com/questions/47671108/docker-php-ext-install-mcrypt-missing-folder

# サーバー構成
・Webサーバー(Nginx)172.18.0.5
・アプリサーバー(PHP, Laravel)172.18.0.4
・DBサーバー(Mysql)172.18.0.2
・キャッシュサーバー(Redis)172.18.0.3
## 通信の流れ
internet <-> nginx <-> php-fpm <-> index.php(laravel) <-> mysql, redis

# めも
・複数のコンテナを稼動する時は`docker-compose`を使うことで一括管理できる
・`docker-compose.yml`で立てるコンテナの設定を行う
・ローカルとファイル同期もできる（永続化）
・既存のコンテナをカスタマイズして立てる場合は`hoge.docker`に処理を書く
・dockerファイルのRUN構文に空行があると今後エラーになるらしい

## docker-composeコマンド
https://qiita.com/aild_arch_bfmv/items/d47caf37b79e855af95f
