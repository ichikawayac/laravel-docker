MacでDocker
# 参考記事
・brewでのインストール
https://qiita.com/digitalm/items/49b7099554bdbff5ed8e
・コンテナ起動など
https://qiita.com/kurkuru/items/127fa99ef5b2f0288b81


# Dockerのインストール
```
brew install docker
brew cask install docker
```
`Docker.app`を起動

確認
```
docker version
```

クライアントとサーバーの情報が表示されれば起動成功
```
Client:
 Version: 17.12.0-ce
 API version: 1.35
 Go version:  go1.9.2
 Git commit:  c97c6d6
 Built: Wed Dec 27 20:03:51 2017
 OS/Arch: darwin/amd64

Server:
 Engine:
  Version:  17.12.0-ce
  API version:  1.35 (minimum version 1.12)
  Go version: go1.9.2
  Git commit: c97c6d6
  Built:  Wed Dec 27 20:12:29 2017
  OS/Arch:  linux/amd64
  Experimental: true
```


# Nginxコンテナの起動
コンテナ名`nginx_test`、ポート８０番を指定して起動する
```
docker run -d -p 80:80 --name nginx_test nginx
```
-d: Run container in background and print container ID
-p: Publish a container's port(s) to the host

起動確認
```
docker container ls
```

```
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                NAMES
2070cbd6269e        nginx               "nginx -g 'daemon of…"   About a minute ago   Up About a minute   0.0.0.0:80->80/tcp   nginx_test
```

# Nginxコンテナの停止/再開

停止
```
docker container stop nginx_test
```

停止中のコンテナも含む一覧表示
```
docker container ls -a
```

再開
```
docker container start nginx_test
```

# コンテナの削除
コンテナを停止してから削除する
```
docker container rm nginx_test
```

# イメージの削除
```
docker image rm nginx
```


出来合いのコンテナを起動する

カスタマイズして起動する


